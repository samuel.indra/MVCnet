﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MVCProject.Models
{
    public class Film
    {
        public Guid movieid { get; set; }
        public string namamovie { get; set; }
        public string genre { get; set; }
        public string tahunrilis { get; set; }
        MovieEntities db = new MovieEntities();
        public List<movie> GetMovies()
        {
            var films = (from x in db.movies
                         select x).ToList();
            return films;
        }
    }
}