﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MVCProject.Models;
using System.Globalization;

namespace MVCProject.Controllers
{
    public class UserController : Controller
    {
        // GET: User
        public ActionResult Login()
        {
            return View();
        }
        
        [HttpPost]
        public ActionResult Login(user col)
        {
            if (ModelState.IsValid)
            {
                using(MovieEntities db = new MovieEntities())
                {
                    var obj = db.systemusers.Where(x => x.username.Equals(col.username) && x.password.Equals(col.password)).FirstOrDefault();
                    if(obj != null)
                    {
                        Session["UserID"] = col.Id.ToString();
                        Session["Username"] = col.username.ToString();
                        return RedirectToAction("Index", "Home");
                    }
                }   
            }
            return View(col);
        }

        

        public ActionResult List()
        {
            user mod = new user();
            ViewBag.dataUser = mod.GetSystemUsers();
            return View();
        }

        public ActionResult AddUser()
        {
            return View();
        }
        [HttpPost]
        public ActionResult AddUser(FormCollection col)
        {
            MovieEntities db = new MovieEntities();
            systemuser sysuser = new systemuser();
            Guid userid = Guid.NewGuid();
            string fullname = col["fullname"];
            string username = col["username"];
            string password = col["password"];
            sysuser.systemUserID = userid;
            sysuser.fullname = fullname;
            sysuser.username = username;
            sysuser.password = password;
            db.systemusers.Add(sysuser);
            db.SaveChanges();
            
            return RedirectToAction("List","User");
        }

        public ActionResult DeleteUser(Guid id)
        {
            MovieEntities db = new MovieEntities();
            user usr = db.systemusers.Where(x => x.systemUserID == id).Select(x => new user()
            {
                Id = x.systemUserID,
                fullname = x.fullname,
                username = x.username,
                password = x.password
            }).SingleOrDefault();

            return View(usr);
        }

        [HttpPost]
        public ActionResult DeleteUser(user col)
        {
            MovieEntities db = new MovieEntities();
            systemuser user = db.systemusers.Where(x => x.systemUserID == col.Id).Single<systemuser>();
            db.systemusers.Remove(user);
            db.SaveChanges();

            return RedirectToAction("List", "User");
        }

        [HttpGet]
        public ActionResult Edit( Guid id)
        {
            MovieEntities db = new MovieEntities();
            user usr = db.systemusers.Where(x => x.systemUserID == id).Select(x => new user
            {
                Id = x.systemUserID,
                fullname = x.fullname,
                username = x.username,
                password = x.password
            }).SingleOrDefault();

            return View(usr);
        }

        public ActionResult Edit(user col)
        {
            MovieEntities db = new MovieEntities();
            systemuser user = db.systemusers.Where(x => x.systemUserID == col.Id).Single<systemuser>();
            user.systemUserID = col.Id;
            user.fullname = col.fullname;
            user.username = col.username;
            user.password = col.password;
            db.SaveChanges();
            return RedirectToAction("List", "User");
        }
    }
}